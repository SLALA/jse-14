package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> list = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return list;
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        final List<Project> projects = new ArrayList<>(list);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public void add(final Project project) {
        list.add(project);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public void remove(final Project project) {
        list.remove(project);
    }

    @Override
    public Project findById(final String id) {
        for (final Project project : list) {
            if (id.equals(project.getId()))
                return project;
        }
        return null;
    }

    @Override
    public Project findByName(final String name) {
        for (final Project project : list) {
            if (name.equals(project.getName()))
                return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index >= list.size()) return null;
        return list.get(index);
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findById(id);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        remove(project);
        return project;
    }

}
