package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.repository.ICommandRepository;
import ru.t1.strelcov.tm.constant.ArgumentConst;
import ru.t1.strelcov.tm.constant.TerminalConst;
import ru.t1.strelcov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Display list of terminal commands.");

    private static final Command ABOUT = new Command(TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Display developer info.");

    private static final Command VERSION = new Command(TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "Display program version.");

    private static final Command INFO = new Command(TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "Display system info.");

    private static final Command EXIT = new Command(TerminalConst.CMD_EXIT, null, "Exit program.");

    private static final Command COMMANDS = new Command(TerminalConst.CMD_COMMANDS, null, "Display commands.");

    private static final Command ARGUMENTS = new Command(TerminalConst.CMD_ARGUMENTS, null, "Display arguments.");

    public static final Command TASK_CREATE = new Command(TerminalConst.CMD_TASK_CREATE, null, "Create task.");

    public static final Command TASK_CLEAR = new Command(TerminalConst.CMD_TASK_CLEAR, null, "Clear tasks.");

    public static final Command TASK_LIST = new Command(TerminalConst.CMD_TASK_LIST, null, "List tasks.");

    public static final Command PROJECT_CREATE = new Command(TerminalConst.CMD_PROJECT_CREATE, null, "Create project.");

    public static final Command PROJECT_CLEAR = new Command(TerminalConst.CMD_PROJECT_CLEAR, null, "Clear projects.");

    public static final Command PROJECT_LIST = new Command(TerminalConst.CMD_PROJECT_LIST, null, "List projects.");

    public static final Command PROJECT_FIND_BY_ID = new Command(TerminalConst.CMD_PROJECT_FIND_BY_ID, null, "Find project by Id.");

    public static final Command PROJECT_FIND_BY_INDEX = new Command(TerminalConst.CMD_PROJECT_FIND_BY_INDEX, null, "Find project by index.");

    public static final Command PROJECT_FIND_BY_NAME = new Command(TerminalConst.CMD_PROJECT_FIND_BY_NAME, null, "Find project by name.");

    public static final Command PROJECT_REMOVE_BY_ID = new Command(TerminalConst.CMD_PROJECT_REMOVE_BY_ID, null, "Remove project by Id.");

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX, null, "Remove project by index.");

    public static final Command PROJECT_REMOVE_BY_NAME = new Command(TerminalConst.CMD_PROJECT_REMOVE_BY_NAME, null, "Remove project by name.");

    public static final Command PROJECT_UPDATE_BY_ID = new Command(TerminalConst.CMD_PROJECT_UPDATE_BY_ID, null, "Update project by Id.");

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX, null, "Update project by index.");

    public static final Command PROJECT_UPDATE_BY_NAME = new Command(TerminalConst.CMD_PROJECT_UPDATE_BY_NAME, null, "Update project by name.");

    public static final Command TASK_FIND_BY_ID = new Command(TerminalConst.CMD_TASK_FIND_BY_ID, null, "Find task by Id.");

    public static final Command TASK_FIND_BY_INDEX = new Command(TerminalConst.CMD_TASK_FIND_BY_INDEX, null, "Find task by index.");

    public static final Command TASK_FIND_BY_NAME = new Command(TerminalConst.CMD_TASK_FIND_BY_NAME, null, "Find task by name.");

    public static final Command TASK_REMOVE_BY_ID = new Command(TerminalConst.CMD_TASK_REMOVE_BY_ID, null, "Remove task by Id.");

    public static final Command TASK_REMOVE_BY_INDEX = new Command(TerminalConst.CMD_TASK_REMOVE_BY_INDEX, null, "Remove task by index.");

    public static final Command TASK_REMOVE_BY_NAME = new Command(TerminalConst.CMD_TASK_REMOVE_BY_NAME, null, "Remove task by name.");

    public static final Command TASK_UPDATE_BY_ID = new Command(TerminalConst.CMD_TASK_UPDATE_BY_ID, null, "Update task by Id.");

    public static final Command TASK_UPDATE_BY_INDEX = new Command(TerminalConst.CMD_TASK_UPDATE_BY_INDEX, null, "Update task by index.");

    public static final Command TASK_UPDATE_BY_NAME = new Command(TerminalConst.CMD_TASK_UPDATE_BY_NAME, null, "Update task by name.");

    public static final Command TASK_START_BY_ID = new Command(TerminalConst.CMD_TASK_START_BY_ID, null, "Start task by Id.");

    public static final Command TASK_START_BY_INDEX = new Command(TerminalConst.CMD_TASK_START_BY_INDEX, null, "Start task by index.");

    public static final Command TASK_START_BY_NAME = new Command(TerminalConst.CMD_TASK_START_BY_NAME, null, "Start task by name.");

    public static final Command TASK_COMPLETE_BY_ID = new Command(TerminalConst.CMD_TASK_COMPLETE_BY_ID, null, "Complete task by Id.");

    public static final Command TASK_COMPLETE_BY_INDEX = new Command(TerminalConst.CMD_TASK_COMPLETE_BY_INDEX, null, "Complete task by index.");

    public static final Command TASK_COMPLETE_BY_NAME = new Command(TerminalConst.CMD_TASK_COMPLETE_BY_NAME, null, "Complete task by name.");

    public static final Command PROJECT_START_BY_ID = new Command(TerminalConst.CMD_PROJECT_START_BY_ID, null, "Start project by Id.");

    public static final Command PROJECT_START_BY_INDEX = new Command(TerminalConst.CMD_PROJECT_START_BY_INDEX, null, "Start project by index.");

    public static final Command PROJECT_START_BY_NAME = new Command(TerminalConst.CMD_PROJECT_START_BY_NAME, null, "Start project by name.");

    public static final Command PROJECT_COMPLETE_BY_ID = new Command(TerminalConst.CMD_PROJECT_COMPLETE_BY_ID, null, "Complete project by Id.");

    public static final Command PROJECT_COMPLETE_BY_INDEX = new Command(TerminalConst.CMD_PROJECT_COMPLETE_BY_INDEX, null, "Complete project by index.");

    public static final Command PROJECT_COMPLETE_BY_NAME = new Command(TerminalConst.CMD_PROJECT_COMPLETE_BY_NAME, null, "Complete project by name.");

    public static final Command PROJECT_REMOVE_WITH_TASKS = new Command(TerminalConst.CMD_PROJECT_REMOVE_WITH_TASKS, null, "Remove project with bound tasks.");

    public static final Command TASK_BIND_TO_PROJECT = new Command(TerminalConst.CMD_TASK_BIND_TO_PROJECT, null, "Bind task to project.");

    public static final Command TASK_UNBIND_FROM_PROJECT = new Command(TerminalConst.CMD_TASK_UNBIND_FROM_PROJECT, null, "Unbind task from project.");

    public static final Command TASK_FIND_ALL_BY_PROJECT = new Command(TerminalConst.CMD_TASK_FIND_ALL_BY_PROJECT, null, "Find all tasks related to project.");

    private static final Command[] TERMINAL_COMMANDS = {
            HELP, ABOUT, VERSION, INFO, COMMANDS, ARGUMENTS,
            TASK_CREATE, TASK_CLEAR, TASK_LIST,
            TASK_FIND_BY_ID, TASK_FIND_BY_INDEX, TASK_FIND_BY_NAME,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_NAME,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_NAME,
            TASK_START_BY_ID, TASK_START_BY_INDEX, TASK_START_BY_NAME,
            TASK_COMPLETE_BY_ID, TASK_COMPLETE_BY_INDEX, TASK_COMPLETE_BY_NAME,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST,
            PROJECT_FIND_BY_ID, PROJECT_FIND_BY_INDEX, PROJECT_FIND_BY_NAME,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_NAME,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_NAME,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX, PROJECT_START_BY_NAME,
            PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_INDEX, PROJECT_COMPLETE_BY_NAME,
            PROJECT_REMOVE_WITH_TASKS, TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT, TASK_FIND_ALL_BY_PROJECT,
            EXIT
    };

    public Command[] getCommands() {
        return TERMINAL_COMMANDS;
    }

}
