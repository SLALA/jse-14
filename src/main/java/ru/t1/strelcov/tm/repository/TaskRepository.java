package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        final List<Task> tasks = new ArrayList<>(list);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public void add(final Task task) {
        list.add(task);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public Task findById(final String id) {
        for (final Task task : list) {
            if (id.equals(task.getId()))
                return task;
        }
        return null;
    }

    @Override
    public Task findByName(final String name) {
        for (final Task task : list) {
            if (name.equals(task.getName()))
                return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (index >= list.size()) return null;
        return list.get(index);
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public void removeAllByProjectId(final String projectId) {
        final List<Task> tasksOfProject = findAllByProjectId(projectId);
        list.removeAll(tasksOfProject);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> tasksOfProject = new ArrayList<>();
        for (final Task task : list) {
            if (projectId.equals(task.getProjectId()))
                tasksOfProject.add(task);
        }
        return tasksOfProject;
    }

}
