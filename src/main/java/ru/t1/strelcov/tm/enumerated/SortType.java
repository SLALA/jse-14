package ru.t1.strelcov.tm.enumerated;

import ru.t1.strelcov.tm.comparator.*;

import java.util.Comparator;

public enum SortType {

    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    START_DATE("Sort by start date", ComparatorByDateStart.getInstance());

    private final String displayName;

    private final Comparator comparator;

    public static boolean isValidByName(final String name) {
        for (final SortType sortType : values()) {
            if (sortType.toString().equals(name))
                return true;
        }
        return false;
    }

    SortType(final String displayName, final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
