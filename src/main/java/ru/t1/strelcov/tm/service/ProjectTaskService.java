package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.api.service.IProjectTaskService;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private ITaskRepository taskRepository;

    private IProjectRepository projectRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        final List<Task> tasksOfProject = taskRepository.findAllByProjectId(projectId);
        return tasksOfProject;
    }

    @Override
    public Task bindTaskToProject(final String taskId, final String projectId) {
        if (taskId == null || taskId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public Project removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        taskRepository.removeAllByProjectId(projectId);
        final Project project = projectRepository.removeById(projectId);
        return project;
    }

}
