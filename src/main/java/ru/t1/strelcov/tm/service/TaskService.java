package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final String name, final String description) {
        final Task task;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty())
            task = new Task(name);
        else
            task = new Task(name, description);
        add(task);
        return task;
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public Task findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        final Task task = taskRepository.findById(id);
        return task;
    }

    @Override
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.findByName(name);
        return task;
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        final Task task = taskRepository.findByIndex(index);
        return task;
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        final Task task = taskRepository.removeById(id);
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.removeByName(name);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        final Task task = taskRepository.removeByIndex(index);
        return task;
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        final Task task = findById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByName(final String oldName, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        final Task task = findByName(oldName);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS)
            task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task changeStatusByName(final String oldName, final Status status) {
        final Task task = findByName(oldName);
        if (task == null) return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS)
            task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS)
            task.setDateStart(new Date());
        return task;
    }

}
