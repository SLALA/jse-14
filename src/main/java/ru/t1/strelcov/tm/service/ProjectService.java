package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project add(final String name, final String description) {
        final Project project;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty())
            project = new Project(name);
        else
            project = new Project(name, description);
        add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public Project findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        final Project project = projectRepository.findById(id);
        return project;
    }

    @Override
    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Project project = projectRepository.findByName(name);
        return project;
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        final Project project = projectRepository.findByIndex(index);
        return project;
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        final Project project = projectRepository.removeById(id);
        return project;
    }

    @Override
    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Project project = projectRepository.removeByName(name);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        final Project project = projectRepository.removeByIndex(index);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        final Project project = findById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByName(final String oldName, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        final Project project = findByName(oldName);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS)
            project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project changeStatusByName(final String oldName, final Status status) {
        final Project project = findByName(oldName);
        if (project == null) return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS)
            project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS)
            project.setDateStart(new Date());
        return project;
    }

}
