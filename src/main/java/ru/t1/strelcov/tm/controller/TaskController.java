package ru.t1.strelcov.tm.controller;

import ru.t1.strelcov.tm.api.controller.ITaskController;
import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static ru.t1.strelcov.tm.enumerated.Status.*;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("[Id]: " + task.getId());
        System.out.println("[Name]: " + task.getName());
        System.out.println("[Description]: " + task.getDescription());
        System.out.println("[Status]: " + task.getStatus().getDisplayName());
        System.out.println("[Project Id]: " + task.getProjectId());
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT FIELD " + Arrays.toString(SortType.values()) + ":");
        List<Task> tasks;
        final String sort = TerminalUtil.nextLine();
        if (sort == null || sort.isEmpty()) tasks = taskService.findAll();
        else {
            if (SortType.isValidByName(sort)) {
                final SortType sortType = SortType.valueOf(sort);
                final Comparator comparator = sortType.getComparator();
                tasks = taskService.findAll(comparator);
                System.out.println(sortType.getDisplayName());
            } else {
                System.out.println("Incorrect sort option.");
                System.out.println("[FAIL]");
                return;
            }
        }
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void findById() {
        System.out.println("[FIND TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void findByName() {
        System.out.println("[FIND TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void findByIndex() {
        System.out.println("[FIND TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        Integer index = TerminalUtil.nextNumber();
        if (index == null) {
            System.out.println("[FAIL]");
            return;
        } else
            index -= 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void removeById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeByName() {
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        Integer index = TerminalUtil.nextNumber();
        if (index == null) {
            System.out.println("[FAIL]");
            return;
        } else
            index -= 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NEW NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateById(id, name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void updateByName() {
        System.out.println("[UPDATE TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        final String oldName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByName(oldName, name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void updateByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        Integer index = TerminalUtil.nextNumber();
        if (index == null) {
            System.out.println("[FAIL]");
            return;
        } else
            index -= 1;
        System.out.println("ENTER NEW NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void startById() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusById(id, IN_PROGRESS);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void startByName() {
        System.out.println("[START TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusByName(name, IN_PROGRESS);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void startByIndex() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        Integer index = TerminalUtil.nextNumber();
        if (index == null) {
            System.out.println("[FAIL]");
            return;
        } else
            index -= 1;
        final Task task = taskService.changeStatusByIndex(index, IN_PROGRESS);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void completeById() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusById(id, COMPLETED);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void completeByName() {
        System.out.println("[COMPLETE TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusByName(name, COMPLETED);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void completeByIndex() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        Integer index = TerminalUtil.nextNumber();
        if (index == null) {
            System.out.println("[FAIL]");
            return;
        } else
            index -= 1;
        final Task task = taskService.changeStatusByIndex(index, COMPLETED);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

}
