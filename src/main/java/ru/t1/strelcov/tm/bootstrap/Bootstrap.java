package ru.t1.strelcov.tm.bootstrap;

import ru.t1.strelcov.tm.api.controller.ICommandController;
import ru.t1.strelcov.tm.api.controller.IProjectController;
import ru.t1.strelcov.tm.api.controller.IProjectTaskController;
import ru.t1.strelcov.tm.api.controller.ITaskController;
import ru.t1.strelcov.tm.api.repository.ICommandRepository;
import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.api.service.ICommandService;
import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.api.service.IProjectTaskService;
import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.constant.ArgumentConst;
import ru.t1.strelcov.tm.constant.TerminalConst;
import ru.t1.strelcov.tm.controller.CommandController;
import ru.t1.strelcov.tm.controller.ProjectController;
import ru.t1.strelcov.tm.controller.ProjectTaskController;
import ru.t1.strelcov.tm.controller.TaskController;
import ru.t1.strelcov.tm.repository.CommandRepository;
import ru.t1.strelcov.tm.repository.ProjectRepository;
import ru.t1.strelcov.tm.repository.TaskRepository;
import ru.t1.strelcov.tm.service.CommandService;
import ru.t1.strelcov.tm.service.ProjectService;
import ru.t1.strelcov.tm.service.ProjectTaskService;
import ru.t1.strelcov.tm.service.TaskService;
import ru.t1.strelcov.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectController projectController = new ProjectController(projectService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    public void run(String... args) {
        displayWelcome();
        if (parseArgs(args))
            commandController.exit();
        while (true) {
            System.out.println("ENTER COMMAND:");
            parseCommand(TerminalUtil.nextLine());
            System.out.println();
        }
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        return true;
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ARG_ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.displaySystemInfo();
                break;
            default:
                System.out.println("Error: Command doesn't exist. Enter command: help");
                break;
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.CMD_INFO:
                commandController.displaySystemInfo();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.CMD_TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.CMD_TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.CMD_TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.CMD_PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.CMD_PROJECT_CLEAR:
                projectController.clear();
                break;
            case TerminalConst.CMD_PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.CMD_PROJECT_FIND_BY_ID:
                projectController.findById();
                break;
            case TerminalConst.CMD_PROJECT_FIND_BY_INDEX:
                projectController.findByIndex();
                break;
            case TerminalConst.CMD_PROJECT_FIND_BY_NAME:
                projectController.findByName();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeById();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeByIndex();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_NAME:
                projectController.removeByName();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_NAME:
                projectController.updateByName();
                break;
            case TerminalConst.CMD_TASK_FIND_BY_ID:
                taskController.findById();
                break;
            case TerminalConst.CMD_TASK_FIND_BY_INDEX:
                taskController.findByIndex();
                break;
            case TerminalConst.CMD_TASK_FIND_BY_NAME:
                taskController.findByName();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_NAME:
                taskController.removeByName();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_NAME:
                taskController.updateByName();
                break;
            case TerminalConst.CMD_TASK_START_BY_ID:
                taskController.startById();
                break;
            case TerminalConst.CMD_TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case TerminalConst.CMD_TASK_START_BY_NAME:
                taskController.startByName();
                break;
            case TerminalConst.CMD_TASK_COMPLETE_BY_ID:
                taskController.completeById();
                break;
            case TerminalConst.CMD_TASK_COMPLETE_BY_INDEX:
                taskController.completeByIndex();
                break;
            case TerminalConst.CMD_TASK_COMPLETE_BY_NAME:
                taskController.completeByName();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_NAME:
                projectController.startByName();
                break;
            case TerminalConst.CMD_PROJECT_COMPLETE_BY_ID:
                projectController.completeById();
                break;
            case TerminalConst.CMD_PROJECT_COMPLETE_BY_INDEX:
                projectController.completeByIndex();
                break;
            case TerminalConst.CMD_PROJECT_COMPLETE_BY_NAME:
                projectController.completeByName();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_WITH_TASKS:
                projectTaskController.removeProjectWithTasks();
                break;
            case TerminalConst.CMD_TASK_FIND_ALL_BY_PROJECT:
                projectTaskController.findAllByProject();
                break;
            case TerminalConst.CMD_TASK_BIND_TO_PROJECT:
                projectTaskController.bindToProject();
                break;
            case TerminalConst.CMD_TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindFromProject();
                break;
            default:
                System.out.println("Error: Command doesn't exist. Enter command: help");
                break;
        }
    }

}
