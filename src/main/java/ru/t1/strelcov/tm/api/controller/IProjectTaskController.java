package ru.t1.strelcov.tm.api.controller;

public interface IProjectTaskController {

    void findAllByProject();

    void bindToProject();

    void unbindFromProject();

    void removeProjectWithTasks();

}
