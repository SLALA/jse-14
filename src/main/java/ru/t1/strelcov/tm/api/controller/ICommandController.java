package ru.t1.strelcov.tm.api.controller;

public interface ICommandController {

    void displayHelp();

    void displayCommands();

    void displayArguments();

    void displayVersion();

    void displayAbout();

    void displaySystemInfo();

    void exit();

}
