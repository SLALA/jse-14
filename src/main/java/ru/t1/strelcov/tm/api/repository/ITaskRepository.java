package ru.t1.strelcov.tm.api.repository;

import ru.t1.strelcov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    void add(Task task);

    void clear();

    void remove(Task task);

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    void removeAllByProjectId(String projectId);

    List<Task> findAllByProjectId(String projectId);

}
