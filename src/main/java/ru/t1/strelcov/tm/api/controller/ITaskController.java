package ru.t1.strelcov.tm.api.controller;

public interface ITaskController {

    void showList();

    void create();

    void clear();

    void findById();

    void findByName();

    void findByIndex();

    void removeById();

    void removeByName();

    void removeByIndex();

    void updateById();

    void updateByName();

    void updateByIndex();

    void startById();

    void startByName();

    void startByIndex();

    void completeById();

    void completeByName();

    void completeByIndex();

}
