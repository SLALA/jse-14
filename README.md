# TASK MANAGER

Console Application for task list.

# DEVELOPER INFO

NAME: Sla La

E-MAIL: slala@slala.ru

# SOFTWARE

* JDK 1.8

* MS Windows

# HARDWARE

* RAM 10 Gb

* CPU i5

* HDD 120 Gb

# BUILD PROGRAM

```
mvn clean install
```

# RUN PROGRAM

```
java -jar ./task-manager.jar
```

# SCREENSHOTS

